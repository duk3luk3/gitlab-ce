require 'spec_helper'

describe CreateUserService, services: true do
  before do
    @user = create :user
    @admin = create :user, admin: true
    @user_params = {
      name: 'Test User',
      username: 'testuser',
      email: 'testuser@example.com'
    }
    @opts_randomuserpassword = {
      force_random_password: true,
      password_expires_at: nil
    }
  end

  it 'creates a confirmed user with a random expired password' do
    expect{ create_user(@admin, @user_params, @opts_randomuserpassword).save }.to change{ User.count }.by(1)
  end

  it 'non-admins are not able to create users' do
    expect{ create_user(@user, @user_params, @opts_randomuserpassword) }.to raise_error(Gitlab::Access::AccessDeniedError)
  end

  def create_user(current_user, user_params, opts)
    CreateUserService.new(current_user, user_params).execute(opts)
  end
end
