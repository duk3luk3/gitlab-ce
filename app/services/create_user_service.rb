class CreateUserParameterError < StandardError; end

class CreateUserService
  attr_accessor :current_user

  def initialize(current_user, params = {})
    raise Gitlab::Access::AccessDeniedError unless current_user.is_admin?
    @current_user = current_user
    @params = params
  end

  def execute(opts = {})
    user_params = @params.merge(opts)
    raise CreateUserParameterError unless (user_params.has_key?(:password) ^ user_params[:force_random_password])

    admin = user_params.delete(:admin)
    confirm = user_params.delete(:confirm)
    generate_password = user_params[:force_random_password]

    user_params[:password_expires_at] = nil if generate_password

    @user = User.new(user_params)
    @user.created_by_id = current_user.id
    @user.generate_reset_token if generate_password
    @user.skip_confirmation! unless confirm
    @user.admin = admin unless admin.nil?

    return @user
  end
end
